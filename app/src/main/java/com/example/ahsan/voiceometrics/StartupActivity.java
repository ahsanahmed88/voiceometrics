package com.example.ahsan.voiceometrics;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.Series;
import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;

import java.util.Random;

public class StartupActivity extends AppCompatActivity {

    Button browse;
    Button start;
    Button cancel;
    TextView textView;

    private LineGraphSeries<DataPoint> series;
    private  static final Random RANDOM = new Random();
    private int LastX = 0;
    private int count = 0;
    public  double [] dp = new double[101];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M  && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new  String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1001);
        }



        browse = (Button) findViewById(R.id.browse);
        cancel = (Button) findViewById(R.id.cancel);
        start = (Button) findViewById(R.id.start);


        textView = (TextView) findViewById(R.id.textView);

        //graph view instance
        GraphView graph = (GraphView) findViewById(R.id.graph);
        //data
        series = new LineGraphSeries<DataPoint>();
        series.setDrawDataPoints(true);
        series.setDataPointsRadius(10);
        graph.addSeries(series);
        //customize the view port
        graph.setTitle("Threat level graph");
        graph.setTitleTextSize(65);
        Viewport viewport =  graph.getViewport();
        viewport.setYAxisBoundsManual(true);
        viewport.setMinY(0);
        viewport.setMaxY(100);
        viewport.setScrollable(true);
        viewport.setXAxisBoundsManual(true);
        viewport.setMinX(0);
        viewport.setMaxX(10);

        //adding headings for axes
        graph.getGridLabelRenderer().setHorizontalAxisTitle("Time(1 second step)");
        graph.getGridLabelRenderer().setHorizontalAxisTitleTextSize(40);
        graph.getGridLabelRenderer().setVerticalAxisTitle("Threat level");
        graph.getGridLabelRenderer().setVerticalAxisTitleTextSize(40);
        graph.getGridLabelRenderer().setPadding(50);

        for (int i =0; i < 101; i++){
            dp [i] = RANDOM.nextDouble() * (100d - 0d) + 0d;
        }


        browse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new MaterialFilePicker()
                        .withActivity(StartupActivity.this)
                        .withRequestCode(1000)
//                        .withFilter(Pattern.compile(".*\\.wav$")) // Filtering files and directories by file name using regexp
//                        .withFilterDirectories(true) // Set directories filterable (false by default)
                        .withHiddenFiles(true) // Show hidden files and folders
                        .start();
            }
        });
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start.setEnabled(false);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        // we add 100 new entries
                        for (int i = 0; i < 101; i++){
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    series.appendData(new DataPoint(LastX++,dp[count]),true,100);
                                    count++;
                                }
                            });

                            //sleep to slow down the addition of entries
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                //manage error..
                            }
                        }
                        series.setOnDataPointTapListener(new OnDataPointTapListener() {
                            @Override
                            public void onTap(Series series, DataPointInterface dataPoint) {
                                Toast.makeText(getApplicationContext(), "Tapped On "+ dataPoint, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }).start();

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1000 && resultCode == RESULT_OK) {
            String filePath;
            if (!data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH).contains(".wav")){
                Toast.makeText(this, "Incorrect Format selected. Select WAV file", Toast.LENGTH_SHORT).show();
            }
            else {
                filePath = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH);
                textView.setText(filePath);
            }
            // Do anything with file

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 1001:{
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }
    }





}
